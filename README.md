Created a Three.js based web page that displays an interactive scene, 
built using 3D graphics techniques containing a robot. 

•	It features user interaction via mouse and keyboard to control the model, along with a limited amount of animation.

•	The scene was comprised of meshes built from Three.js geometry such as cubes, spheres, cylinders etc.

•	The objects featured in the scene included material properties and textures along with light sources.

•	Three.js plug-in controllers were used to enhance the scene and was coded in JavaScript/HTML.

Scene, Lighting, Shapes, Materials/Textures, Interaction, Robot Model, Shapes, Interaction & Animation